package com.homeworks.hw1;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class GuessGame {
    private int lowerBoundary;
    private int upperBoundary;

    public GuessGame(int lowerBoundary, int upperBoundary) {
        setBoundaries(lowerBoundary, upperBoundary);
    }

    public void setBoundaries(int lowerBoundary, int upperBoundary) {
        this.lowerBoundary = lowerBoundary;
        this.upperBoundary = upperBoundary;
    }

    public void run() {
        int inputNumber = 0;
        int triesCount = 0;
        Scanner input = new Scanner(System.in);
        Random random = new Random();
        int guessNumber = random.nextInt(upperBoundary) + lowerBoundary + 1;

        System.out.println("Welcome to the Guessing Game!");
        System.out.println("The computer has come up with a number between "
                + lowerBoundary + " and " + upperBoundary + ".");
        System.out.println("Try to guess this number!\n");

        do {

            System.out.println("Enter the number: ");
            try {
                inputNumber = input.nextInt();
                ++triesCount;

                if (inputNumber > guessNumber) {
                    System.out.println("Too high!");
                } else if (inputNumber < guessNumber) {
                    System.out.println("Too low!");
                }
            } catch (InputMismatchException e) {
                System.out.println("Incorrect input format! Please enter an integer value.");
                input.next();
            }
        } while (inputNumber != guessNumber);

        System.out.println("Congratulations! You guessed!");
        System.out.println("Revealed number: " + guessNumber);
        System.out.println("Tries count: " + triesCount);
    }
}
